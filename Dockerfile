FROM golang:1.14.6-alpine3.12

ENV swimmer ""
ENV webhook ""
ENV interval 60

WORKDIR /go/src/app
COPY . .

RUN go build

CMD ["sh", "-c", "baywatch -swimmer=${swimmer} -webhook=${webhook} -interval=${interval}"]
