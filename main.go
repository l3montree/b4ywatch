package main

import (
	"baywatch/utils"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

type App struct {
	webhook string
	swimmer []string
	interval int
	logFile *os.File
}

var app = App{}

func getSwimmer(oceanPtr *string) []string {
	ocean := strings.Split(*oceanPtr, ",")
	// lets check if we got something from the command line args
	if len(ocean) == 0 {
		log.Fatalln("No swimmer found. Please add an environment Variable SWIMMER with a comma separated list of urls")
	}

	// check if each swimmer is a valid url
	for _, swimmer := range ocean {
		if !utils.IsValidUrl(swimmer) {
			log.Fatalln(fmt.Sprintf("Could not start service. %s is not a valid URL", swimmer))
		}
	}
	return ocean
}

func getWebhook(webhookPtr *string) string {
	webhook := *webhookPtr
	if len(webhook) == 0 || !utils.IsValidUrl(webhook) {
		log.Fatalln(fmt.Sprintf("Could not start service. Webhook: %s is not valid", webhook))
	}
	return webhook
}

func install() {
	logFile, err := os.OpenFile("baywatch.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
		panic("Could not open log file")
	}
	log.SetOutput(logFile)
	app.logFile = logFile
	// initialize all flags
	oceanFlagValue := flag.String("swimmer", "", "The urls to keep an eye on.")
	webhookPtr := flag.String("webhook", "", "The url to call when something goes wrong.")
	intervalPtr := flag.Int("interval", 60, "The amount of seconds to wait till calling the urls again.")

	flag.Parse()

	app.webhook = getWebhook(webhookPtr)
	// get all URLS of the swimmer - we are supposed to keep an eye on.
	app.swimmer = getSwimmer(oceanFlagValue)
	app.interval = *intervalPtr

	log.Print(fmt.Sprintf("Starting with following configuration:\nSwimmer:%s\nWebhook:%s\nSeconds between calls:%d\n\n", *oceanFlagValue, *webhookPtr, *intervalPtr))
}

func checkIfDrowning() {
	for _, swimmer := range app.swimmer {
		if resp, err := http.Get(swimmer); err == nil {
			var message = fmt.Sprintf("%s responded with status code: %d",swimmer, resp.StatusCode)
			if resp.StatusCode > 299 {
				var body = fmt.Sprintf(`{"text":"[%s] %s"}`, time.Now().Format("2006-02-01 15:04:05"), message)
				_, _ = http.Post(app.webhook, "application/json", strings.NewReader(body))
			}
			log.Println(message)
			_ = resp.Body.Close()
		} else {
			var body = fmt.Sprintf(`{"text":"[%s] %s"}`, time.Now().Format("2006-02-01 15:04:05"), utils.SerializeForJSONResponse(err.Error()))
			_, _ = http.Post(app.webhook, "application/json", strings.NewReader(body))
			log.Println(body)
		}
	}
}

func watch() {
	tick := time.Tick(60 * time.Second)
	for range tick {
		checkIfDrowning()
	}
}

func main() {
	install()
	watch()
	defer app.logFile.Close()
}
