package utils

import (
	"net/url"
	"strings"
)

func IsValidUrl(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	}
	u, err := url.Parse(toTest)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}
	return true
}

func SerializeForJSONResponse(toSerialize string) string {
	return strings.ReplaceAll(toSerialize, "\"", "")
}
