<div align="center">
<a href="https://l3montree.com">
<img align="center" width="150" src="assets/baywatch.png" /></a>

Baywatch notifies a user via different channels, that a website is not reachable. - No successful 2xx Status

</div>

# B4ywatch

# Description
The baywatch application keeps track of different endpoints - called swimmer and their corresponding response codes.

If the request fails it will log this into baywatch.log and trigger a notification to a web hook with the corresponding information.
# Usage
There are multiple ways to run this this application
## Installation
### 1. Compile from sources
Clone the repository wherever you want to run the application:
```shell script
git clone git@gitlab.com:l3montree/microservices/monitoring-server.git
```
Compile the source code:
```
go build
```
After this command a new file called `baywatch` should exist in the directory

### 2. Docker
To run the docker container just paste the following command in your terminal:
```shell script
docker run registry.gitlab.com/l3montree/baywatch
```

## Configuration
The configuration is done with command line flags.
Variables:
1. **swimmer**: A comma separated list of URLs the baywatch application should keep track of
2. **webhook**: A single URL to call when an ERROR occurs. Baywatch will only call the url for a response status code greater than `299`.
3. **interval**: A number of seconds between url calls.

## Example Usage
### Command line usage
```shell script
./baywatch -swimmer="https://l3montree.com,https://stamplab.de" -webhook="https://webhook.url.de" -interval=60
```
### Docker usage
The options are passed inside the docker container with environment variables:
```shell script
docker run registry.gitlab.com/l3montree/baywatch -e swimmer="https://l3montree.com,https://stamplab.de" -e webhook="https://webhook.url.de" -e interval=60
```
# License
Open source under the [MIT](LICENSE) License
___
**Picked from** [l3montree](https://l3montree.com)
